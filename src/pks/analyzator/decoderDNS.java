package pks.analyzator;

import java.util.ArrayList;

import javax.swing.JTextArea;

public class decoderDNS extends abstractClass{
	ArrayList<ArrayList<String>> dnsList = new ArrayList<ArrayList<String>>();
	ArrayList<String> col;
	private String[] frame;
	private int index;	
	private int size;
	private String type;
	private String protocol_ip_type;
	String port_type;
	
	public void setFrame(String[] frame, int index, int size, String type, String protocol_ip_type, String port_type){
		this.frame = frame;
		this.index = index;
		this.size = size;
		this.type = type;
		this.protocol_ip_type = protocol_ip_type;
		this.port_type = port_type;
	}

	public void showDNS(){	
		String dest_port = getDecimalPort(frame[36], frame[37])+"";
		String source_port = getDecimalPort(frame[34], frame[35])+"";
		
		if(dest_port.equals(port_type) || source_port.equals(port_type)){
		col = new ArrayList<String>();
		col.add(getSenderIP(frame,26,30));//0
		col.add(getTargetIP(frame,30,34));//1
		col.add((index+1)+"");//2
		col.add(mergeFrame(frame));//3
		col.add(size+"");//4
		col.add(zratajPrenosMedium(size)+"");//5
		col.add(getDecimalPort(frame[36],frame[37])+"");//6
		col.add(getDecimalPort(frame[34],frame[35])+"");//7
		col.add(type);//8
		col.add(protocol_ip_type);//9
		col.add("DNS "+port_type);//10
		
		dnsList.add(col);
		
		}
	}
	
	public void printAllDNS(JTextArea textArea){
		
		for(int i = 0; i < dnsList.size(); i++){
			textArea.append("Zdrojova IP: " + dnsList.get(i).get(0) +", ");
			textArea.append("Cielova IP: " +dnsList.get(i).get(1)+ "\n");
			textArea.append("Zdrojovy port: " + dnsList.get(i).get(7) + ", ");
			textArea.append("Cielovy port: " + dnsList.get(i).get(6)  +"\n");
			textArea.append(dnsList.get(i).get(9)+"\n");
			textArea.append("Ramec c." +dnsList.get(i).get(2) + "\n");
			textArea.append("dĺžka rámca zachyteného paketovým drajverom: " +dnsList.get(i).get(4) + "\n");
			textArea.append("dĺžka rámca prenášaného po médiu: " + dnsList.get(i).get(5)+"\n");
			textArea.append("Ethernet II, "+dnsList.get(i).get(8)+"\n");
			textArea.append("Well-known port: "+dnsList.get(i).get(10)+"\n");
			textArea.append(dnsList.get(i).get(3) + "\n\n");
		}
		
		clearAllDNS();
		
	}
	
	public void clearAllDNS(){
		dnsList.clear();
		
	}
}
