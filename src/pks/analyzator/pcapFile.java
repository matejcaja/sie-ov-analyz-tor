package pks.analyzator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileReader;
import java.util.ArrayList;

import javax.swing.JTextArea;

public class pcapFile {
JTextArea textArea;
ArrayList<ArrayList<String>> fileList = new ArrayList<ArrayList<String>>();	
ArrayList<String> c;
ArrayList<ArrayList<String>> portList = new ArrayList<ArrayList<String>>();	
ArrayList<ArrayList<String>> protocolList = new ArrayList<ArrayList<String>>();	
	
	public void createIpProtocolList() throws IOException {
		FileReader file = new FileReader(
				"~/workspace/PKS/pcap/ipProtocolNumbers.txt");
		BufferedReader in = new BufferedReader(file);

		String line;

		while ((line = in.readLine()) != null) {
			c = new ArrayList<String>();
			String[] info = line.split("\t");
			c.add(info[0]);
			c.add(info[1]);
			protocolList.add(c);
		}

		file.close();
	}

	public void createWellKnownPortsList() throws IOException {
		FileReader file = new FileReader(
				"~/workspace/PKS/pcap/wellKnownPorts.txt");
		BufferedReader in = new BufferedReader(file);

		String line;

		while ((line = in.readLine()) != null) {
			c = new ArrayList<String>();
			String[] info = line.split("\t");
			c.add(info[0]);
			c.add(info[1]);
			portList.add(c);
		}

		file.close();
	}
	public void createEtherTypeList() throws IOException {
		FileReader file = new FileReader(
				"~/workspace/PKS/pcap/EtherType.txt");
		BufferedReader in = new BufferedReader(file);

		String line;

		while ((line = in.readLine()) != null) {
			c = new ArrayList<String>();
			String[] info = line.split("\t");
			c.add(info[0]);
			c.add(info[1]);
			fileList.add(c);
		}

		file.close();
	}

	public String etherTypeList(String type, int index){
		for(int i = 0; i < fileList.size(); i++){
			if(fileList.get(i).get(1).equals(type)){
				type = fileList.get(i).get(index);
				return type;
			}
		}
		
				
		return type;
	}
	
	public String wellKnownPortsList(String type, int index){
			
		for(int i = 0; i < portList.size(); i++){
			if(portList.get(i).get(1).equals(type)){
				type = portList.get(i).get(index);
				return type;
			}
		}
		
				
		return null;
	}
	
	public String IpProtocolList(String type, int index){
				
		for(int i = 0; i < protocolList.size(); i++){
			if(protocolList.get(i).get(1).equals(type)){
				type = protocolList.get(i).get(index);
				
				return type;
			}
		}
				
		return null;
	}
		
	public void doStuffWithFiles(){
		try {
			createEtherTypeList();
			createWellKnownPortsList();
			createIpProtocolList();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
