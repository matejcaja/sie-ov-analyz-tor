package pks.analyzator;


import java.util.ArrayList;

import javax.swing.JTextArea;

public class packetDecoder {
	String stype;
	String ethertype;
	String protocol_ip_type;
	String protocol_ip_type2;
	String port_type;
	private String buttonText;
	private JTextArea textArea;
	decoderARP ARP = new decoderARP();
	decoderTFTP TFTP = new decoderTFTP();
	decoderDNS DNS = new decoderDNS();
	ArrayList<Object> decoderList = new ArrayList<Object>();
	pcapFile file = new pcapFile();
	
	public packetDecoder(JTextArea textArea){
		this.textArea = textArea;
	}
	
	public void setButton(String buttonText){
		this.buttonText = buttonText;
	}
	
	public String getButton(){
		return this.buttonText;
	}
	
	
	public void zratajPrenos(int i){
		int jump = 4;
		int size = 0;
		
		String frame = (String) decoderList.get(i);
		frame = frame.trim();
		String[] subframe = frame.split(" ");
			
		int z = subframe.length;
		String ultraframe[] = new String[z];
		for(int j = 0; j < subframe.length; j++){
			if(j != jump){
				ultraframe[size] = subframe[j];
				size++;
			}
			else
				jump += 5;
		}	
			stringFrame(ultraframe,size,i);
		
	}	
	
	public void stringFrame(String[] stringframe, int size, int i){
		String type = "0x"+stringframe[12] + stringframe[13]; 
		String ip_type = Integer.parseInt(stringframe[23], 16)+"";
		int decimal = Integer.parseInt(stringframe[12] + stringframe[13], 16);
		
		if(decimal > 1500){
			ethertype = file.etherTypeList(type, 1);
			
			if(getButton().equals("ARP")){
				ethertype = file.etherTypeList("ARP", 0);//zisti port 
				if(ethertype.equals(type)){//porovnaj port zo suboru s portom vo frame-e
					ethertype = file.etherTypeList("ARP", 1);
					ARP.setFrame(stringframe, i, size, ethertype);
					ARP.decodeList();
				}
			}
			if(getButton().equals("TFTP")){
				ethertype = file.etherTypeList("IPv4", 0);
				if(ethertype.equals(type)){//je to IP
					protocol_ip_type = file.IpProtocolList("UDP", 0);
					if(protocol_ip_type.equals(ip_type)){//je to UDP 
						ethertype = file.etherTypeList("IPv4", 1);
						protocol_ip_type = file.IpProtocolList("UDP", 1);//udp
						port_type = file.wellKnownPortsList("TFTP", 0);//tftp
						TFTP.setFrame(stringframe, i, size, ethertype, protocol_ip_type, port_type);
						TFTP.doStuffWithTFTP(decoderList);
					}
					
				}
			}
			if(getButton().equals("DNS")){
				ethertype = file.etherTypeList("IPv4", 0);
				if(ethertype.equals(type)){//je to IP
					protocol_ip_type = file.IpProtocolList("UDP", 0);
					protocol_ip_type2 = file.IpProtocolList("TCP", 0);
					if(protocol_ip_type.equals(ip_type) || protocol_ip_type.equals(ip_type)){
						ethertype = file.etherTypeList("IPv4", 1);
						protocol_ip_type = file.IpProtocolList("UDP", 1);//udp
						if(protocol_ip_type == null)protocol_ip_type = file.IpProtocolList("TCP", 1);//tcp
						port_type = file.wellKnownPortsList("DNS", 0);
						DNS.setFrame(stringframe, i, size, ethertype, protocol_ip_type, port_type);
						DNS.showDNS();
					}
				}
			}
		}
	}
	
	void showFrames(){
		file.doStuffWithFiles();

		for(int i = 0; i < decoderList.size(); i++){
				zratajPrenos(i);
		}
	}
		
	public void showTFTP(){
		
		showFrames();
		TFTP.addLast();
		TFTP.printAllTFTP(textArea);
	}
	
	public void showARP(){
		
		showFrames();
		ARP.printAllARP(textArea);
		ARP.clearAllARP();

	}
	
	public void showDNS(){
		
		showFrames();
		DNS.printAllDNS(textArea);
		
	}
		
}
