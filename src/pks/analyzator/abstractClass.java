package pks.analyzator;

public abstract class abstractClass {
	pcapFile file = new pcapFile();
	
	
	public int zratajPrenosMedium(int mediumSize){
		if (mediumSize < 64){
			if((mediumSize + 4) > 64 )
				return(mediumSize+4);
			else
				return(mediumSize + (64-mediumSize));	
		}
		else 
			return(mediumSize+4);
	
	}
	
	public String mergeFrame(String[] frame){
		String merge = "";
		
		for(int i = 0; i < frame.length; i++){
			if(frame[i] != null){
				merge += frame[i] +  " ";
			
			if((i+1) % 16 == 0 && i != 0){
				merge += "\n";
			}
			}
		}
		
		return merge;
	}
	
	public String getTargetIP(String[] frame, int a, int b){
		String result;
		Integer[] decimal = new Integer[4];
		int j = 0;
		
		for(int i = a; i < b; i++){
			decimal[j] = Integer.parseInt(frame[i], 16);
			j++;
			
		}
		result = ""+decimal[0]+"."+decimal[1]+"."+decimal[2]+"."+decimal[3];
		
		return result;
	}
	
	public String getSenderIP(String[] frame, int a, int b){
		String result = null;
		Integer[] decimal = new Integer[4];
		int j = 0;
		
		for(int i = a; i < b; i++){
			decimal[j] = Integer.parseInt(frame[i], 16);
			j++;
			
		}
		result = ""+decimal[0]+"."+decimal[1]+"."+decimal[2]+"."+decimal[3];
		
		return result;
	}
	
	public int getDecimalPort(String frame1, String frame2){
		return (Integer.parseInt((frame1+frame2), 16));
	}
}
