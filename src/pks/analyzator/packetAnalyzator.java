package pks.analyzator;
import java.util.ArrayList;
import javax.swing.JTextArea;


public class packetAnalyzator extends abstractClass{
	private JTextArea textArea;
	private String stype;
	ArrayList<Object> analyzatorList = new ArrayList<Object>();
	pcapFile file = new pcapFile();
	
	packetAnalyzator(JTextArea textArea){
		this.textArea = textArea;
	}
	
	void showFrames(){
		for(int i = 0; i < analyzatorList.size(); i++){
			int size = zratajPrenos(i);
			int mediumSize = zratajPrenosMedium(size);
			textArea.append("Rámec: " + (i+1) + "\n");
			textArea.append("dĺžka rámca zachyteného paketovým drajverom: " + size + "\n");
			textArea.append("dĺžka rámca prenášaného po médiu: "+ mediumSize +"\n");
			textArea.append("Typ ramca: "+getString()+"\n");
			textArea.append(analyzatorList.get(i) + "\n");
		}
		textArea.setCaretPosition(0);
	}

	public void setString(String stype){
		this.stype = stype;
	}
	
	public String getString(){
		return this.stype;
	}

	public int zratajPrenos(int i){
		int jump = 4;
		int size = 0;
		String frame = (String) analyzatorList.get(i);
		frame = frame.trim();
		String[] subframe = frame.split(" ");
			
		int z = subframe.length;
		String ultraframe[] = new String[z];
		for(int j = 0; j < subframe.length; j++){
			if(j != jump){
				ultraframe[size] = subframe[j];
				size++;
			}
			else
				jump += 5;
		}	
			stringFrame(ultraframe);
			return size;
		
	}	
	
	public void stringFrame(final String[] stringframe){
		String protocol = null;
		String type = stringframe[12] + stringframe[13]; 
		int decimal = Integer.parseInt(type, 16);
		int ieee_type;
		
		if(decimal <= 1500){
			ieee_type = Integer.parseInt(stringframe[14], 16) + Integer.parseInt(stringframe[15], 16);
			if(ieee_type == 340 ){
				protocol = "IEEE 802.3/SNAP";
			}
			else if(ieee_type == 510){
				protocol = "IEEE 802.3/RAW";	
			}
			else{
				protocol = "IEEE 802.3/LLC";
			}
		}	
		else if(decimal > 1500){
			protocol = "Ethernet II";
			
		
		}
		
		setString(protocol);
	}

	
}
