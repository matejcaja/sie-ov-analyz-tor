package pks.analyzator;
import java.util.ArrayList;

import javax.swing.JTextArea;

public class decoderARP extends abstractClass{
	ArrayList<ArrayList<String>> arpRequest = new ArrayList<ArrayList<String>>();
	ArrayList<ArrayList<String>> arpReply = new ArrayList<ArrayList<String>>();
	private String[] frame;
	private int index;	
	private int size;
	private String type;
	
	public void setFrame(String[] frame, int index, int size, String type){
		this.frame = frame;
		this.index = index;
		this.size = size;
		this.type = type;
	}
		
	public String getTargetMAC(String[] frame){
		String result = frame[32]+":"+frame[33]+":"+frame[34]+":"+frame[35]+":"+frame[36]+":"+frame[37];
		
		if(result.equals("00:00:00:00:00:00"))
			return "???";
		else
			return frame[32]+":"+frame[33]+":"+frame[34]+":"+frame[35]+":"+frame[36]+":"+frame[37];
	}
	
	public String getSenderMAC(String[] frame){
		return frame[22]+":"+frame[23]+":"+frame[24]+":"+frame[25]+":"+frame[26]+":"+frame[27];
	}
	
	public void clearAllARP(){
		arpReply.clear();
		arpRequest.clear();
		
	}
	
	public int parser(String a){
		return Integer.parseInt(a);
	}
	
	public void decodeList(){
		String result = (frame[20]+frame[21]);
		
		if(("0001").equals(result)){
			String s_index = ""+(index+1);
			String s_size = ""+size;
						
			ArrayList<String> col = new ArrayList<String>();
			col.add(getSenderIP(frame,28,32));
			col.add(getTargetIP(frame,38,42));
			col.add(s_index);
			col.add(mergeFrame(frame));
			col.add(getTargetMAC(frame));
			col.add(getSenderMAC(frame));
			col.add(s_size);
			col.add(zratajPrenosMedium(size)+"");
			col.add(type);
			
			arpRequest.add(col);
				
		}
		else if(("0002").equals(result)){
			String s_index = ""+(index+1);
			String s_size = ""+size;
				
			ArrayList<String> col = new ArrayList<String>();
			col.add(getSenderIP(frame,28,32));//0
			col.add(getTargetIP(frame,38,42));//1
			col.add(s_index);//2
			col.add(mergeFrame(frame));//3
			col.add(getTargetMAC(frame));//4
			col.add(getSenderMAC(frame));//5
			col.add(s_size);//6
			col.add(zratajPrenosMedium(size)+"");//7
			col.add(type);//8
			
			arpReply.add(col);
			
		}
	
	}
	
	
	
	public int[][] matchList(){
		
		int[][] indexy = new int[arpReply.size()][2];
		int k = 0;
		
		for(int i = 0; i < arpReply.size(); i++){
			for(int j = 0; j < arpRequest.size(); j++){
				if(arpRequest.get(j).get(0).equals(arpReply.get(i).get(1))){
					if(parser(arpRequest.get(j).get(2)) < parser(arpReply.get(i).get(2))){//ak je cislo ramca request-u mensie ako reply-u, tak match-uj
						indexy[k][0] = i;//reply index
						indexy[k][1] = j;//request index
					}
				}
			}
			k++;
		}

		if(arpRequest.isEmpty() == true || arpReply.isEmpty() == true){
			return null;
		}
		
		return indexy;
	}
	
	public void printAllARP(JTextArea textArea){
		int[][] indexy = matchList();
		if(indexy == null)return;
		
		for(int k = 0; k < arpReply.size(); k++){
			textArea.append("Komunikacia c."+ (k+1) +"\n");
			textArea.append("ARP-Request, IP adresa: " + arpRequest.get(indexy[k][1]).get(1) + ", MAC adresa: " + arpRequest.get(indexy[k][1]).get(4)+"\n");
			textArea.append("Zdrojova IP: " + arpRequest.get(indexy[k][1]).get(0) + ", Cielova IP: " + arpRequest.get(indexy[k][1]).get(1) + "\n");
			textArea.append("ramec " + arpRequest.get(indexy[k][1]).get(2) + "\n");
			textArea.append("dlzka ramca zachytena paketovym drajverom - " + arpRequest.get(indexy[k][1]).get(6) + " B\n");
			textArea.append("dlzka ramca prenasaneho po mediu - " + arpRequest.get(indexy[k][1]).get(7)+" B\n");
			textArea.append("Ethernet II, " + arpRequest.get(indexy[k][1]).get(8)+"\n");
			textArea.append(arpRequest.get(indexy[k][1]).get(3) + "\n\n");
			
			textArea.append("Komunikacia c."+ (k+1) +"\n");
			textArea.append("ARP-Reply, IP adresa: " + arpReply.get(indexy[k][0]).get(0) + ", MAC adresa: " + arpReply.get(indexy[k][0]).get(5)+"\n");
			textArea.append("Zdrojova IP: " + arpReply.get(indexy[k][0]).get(0) + ", Cielova IP: " + arpReply.get(indexy[k][0]).get(1) + "\n");
			textArea.append("ramec " + arpReply.get(indexy[k][0]).get(2) + "\n");
			textArea.append("dlzka ramca zachytena paketovym drajverom - " + arpReply.get(indexy[k][0]).get(6) + " B\n");
			textArea.append("dlzka ramca prenasaneho po mediu - " + arpReply.get(indexy[k][0]).get(7)+" B\n");
			textArea.append("Ethernet II, "+ arpReply.get(indexy[k][0]).get(8)+ "\n");
			textArea.append(arpReply.get(indexy[k][0]).get(3)+"\n\n");
			
		}
		
		clearAllARP();
		
		
	}

}
