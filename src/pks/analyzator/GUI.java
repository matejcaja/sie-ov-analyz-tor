package pks.analyzator;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
import org.jnetpcap.Pcap;  
import org.jnetpcap.packet.PcapPacket;  
import org.jnetpcap.packet.PcapPacketHandler; 


import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import java.io.File;
import javax.swing.JTextField;
import javax.swing.JToolBar;


public class GUI extends JFrame {

	private static final long serialVersionUID = 2387693984237232261L;
//GUI
	private JPanel contentPane = new JPanel();
	final JFileChooser fc = new JFileChooser();
	private JPanel card1 = new JPanel();
	private JPanel card2 = new JPanel();
	private JPanel card3 = new JPanel();
	
	JMenuBar menuBar = new JMenuBar();
	JMenu menu, submenu1;
	JMenuItem menuItem,menuItem2,menuItem3, menuItem4, menuItem5;
	JToolBar toolBar = new JToolBar();
	JButton tlacidlo = new JButton("Open file...");
	JButton tlacidlo2 = new JButton("File menu");
	private CardLayout cardLayout = new CardLayout(0, 0);
	private final JTextField textField = new JTextField();
	private final JButton btnFind = new JButton("Find");
	private final JButton btnNext = new JButton("Next");
	private final JScrollPane scrollPane = new JScrollPane();
	JTextArea textArea = new JTextArea();
	private final JTextField textField_1 = new JTextField();
//GUI	
	
	JTextPane txtpnPacketAnalyzatorc = new JTextPane();
	packetDecoder decoder = new packetDecoder(textArea);
	packetAnalyzator analyzator = new packetAnalyzator(textArea);
	decoderARP ARP = new decoderARP();
	
	File fileChoose;
	Pcap pcap;
	int positionA;
	int def;
	

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public GUI() {
		textField_1.setBounds(23, 75, 492, 19);
		textField_1.setColumns(10);
		textField.setBounds(365, 25, 150, 19);
		textField.setColumns(10);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 555, 526);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		card2.setLayout(null);
		tlacidlo2.setBounds(413, 449, 102, 25);
		card2.add(tlacidlo2);
		contentPane.setLayout(cardLayout);
		contentPane.add(card1, "1");
		contentPane.add(card2, "2");
		contentPane.add(card3, "3");
		card3.setLayout(null);
		scrollPane.setBounds(23, 100, 492, 345);
		card2.add(scrollPane);
		card1.setLayout(null);
		card1.add(tlacidlo);
		tlacidlo.setBounds(218, 194, 113, 25);
		txtpnPacketAnalyzatorc.setEditable(false);
		txtpnPacketAnalyzatorc.setBounds(104, 422, 394, 64);
		txtpnPacketAnalyzatorc.setBackground(null);
		card1.add(txtpnPacketAnalyzatorc);
		scrollPane.setViewportView(textArea);
		card2.add(textField);
		btnFind.setBounds(365, 45, 75, 25);
		card2.add(btnFind);
		btnNext.setBounds(441, 45, 75, 25);
		card2.add(btnNext);
		
		txtpnPacketAnalyzatorc.setText("\tPacket Analyzator (c) Matej Caja\n\t\tBeta verzia\n         Počítačové a komunikačné siete  2012/2013");
		textArea.setFont(new Font("Monospaced", Font.PLAIN, 12));
		textArea.setEditable(false);
		
		toolBar.setBounds(0, 0, 554, 19);
		card2.add(toolBar);
		menu = new JMenu("Program Menu");
		toolBar.add(menuBar);
		menuBar.add(menu);
		menuItem = new JMenuItem("ARP");
		menuItem2 = new JMenuItem("DNS");
		menuItem3 = new JMenuItem("TFTP");
		menuItem4 = new JMenuItem("Home");
		menu.add(menuItem);
		menu.add(menuItem2);
		menu.add(menuItem3);
		menu.addSeparator();
		menu.add(menuItem4);
		card2.add(textField_1);
		textField_1.setBackground(null);
		textField_1.setEditable(false);
		
		btnFind.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					positionA = textArea.getText().toUpperCase().indexOf(textField.getText().toUpperCase());
					def = positionA;
					textArea.setCaretPosition(positionA);
					textArea.getCaretPosition();
					btnNext.setEnabled(true);
				}catch(IllegalArgumentException a){
					JOptionPane.showMessageDialog(contentPane, "No Result!","Find info", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		
			
		btnNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					positionA = textArea.getText().toUpperCase().indexOf(textField.getText().toUpperCase(), positionA+1);
					textArea.setCaretPosition(positionA);
					textArea.getCaretPosition();
				
					if(positionA < 0){
						positionA = def;
					}
				}catch(IllegalArgumentException a){
					JOptionPane.showMessageDialog(contentPane, "No Result!","Find info", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		
		menuItem4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(pcap == null){
					JOptionPane.showMessageDialog(contentPane, "Error while reading .pcap file!","File error!", JOptionPane.ERROR_MESSAGE);
				}
				else{
					textArea.setText("");
					analyzator.showFrames();
					
				}
			}
		});
		
		tlacidlo2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				analyzator.analyzatorList.removeAll(analyzator.analyzatorList);
				decoder.decoderList.removeAll(decoder.decoderList);
				pcap = null;
				textField.setText("");
				textArea.setText("");
				cardLayout.show(contentPane, "1");
			}
		});
		
		
		
		tlacidlo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (arg0.getSource() == tlacidlo){
					int returnVal = fc.showOpenDialog(contentPane);
					if (returnVal == JFileChooser.APPROVE_OPTION) {
						fileChoose = fc.getSelectedFile();
		                getPcapFile(fileChoose.getAbsolutePath());
		            }
				}
				if(pcap != null){
					JOptionPane.showMessageDialog(contentPane, "File loaded succesfully!","File", JOptionPane.INFORMATION_MESSAGE);
					textField_1.setText("Processing file: " +fileChoose.getPath());
				}
			}
		});
		
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea.setText("");
				decoder.setButton(menuItem.getText());				
				decoder.showARP();
			}
		});
		
		menuItem3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea.setText("");
				decoder.setButton(menuItem3.getText());
				decoder.showTFTP();
			}
		});
		
		menuItem2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea.setText("");
				decoder.setButton(menuItem2.getText());
				decoder.showDNS();
			}
		});
	}
	
	void getPcapFile(String file){
		StringBuilder errbuf = new StringBuilder();
		
		pcap = Pcap.openOffline(file, errbuf);
		
		if (pcap == null) {  
        	JOptionPane.showMessageDialog(contentPane, errbuf.toString(),"Find error!", JOptionPane.ERROR_MESSAGE);
           return;  
        } 
		cardLayout.show(contentPane, "2");
		
		PcapPacketHandler<String> jpacketHandler = new PcapPacketHandler<String>() {  
	           public void nextPacket(PcapPacket packet, String user) {  
	        	   analyzator.analyzatorList.add(packet.toHexdump(packet.size(), false, false, true));
	        	   decoder.decoderList.add(packet.toHexdump(packet.size(), false, false, true));
	           }  
	       };  
	       try{
				pcap.loop(-1, jpacketHandler,"");
	       }finally{
	       	pcap.close(); 
	       }
	}
}
