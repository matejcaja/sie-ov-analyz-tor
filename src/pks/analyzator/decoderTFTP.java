package pks.analyzator;

import java.util.ArrayList;

import javax.swing.JTextArea;

public class decoderTFTP extends abstractClass{
	ArrayList<ArrayList<decoderTFTP>> tftpList = new ArrayList<ArrayList<decoderTFTP>>();
	ArrayList<String> portList = new ArrayList<String>();
	ArrayList<decoderTFTP> c = new ArrayList<decoderTFTP>();
	private String[] frame;
	private String mergedFrame;
	private int index;	
	private int size;
	private int source_port;
	private int dest_port;
	private String sourceIP;
	private String destIP;
	private int mediumsize;
	private String type;
	String protocol_ip_type;
	String port_type;
	
	public decoderTFTP(){
		
	}
	
	public decoderTFTP(String[] frame, int index, int size, String type, String protocol_ip_type,String port_type){
		this.index = index+1;
		this.size = size;
		mergedFrame = mergeFrame(frame);
		this.source_port = getDecimalPort(frame[34],frame[35]);
		this.dest_port = getDecimalPort(frame[36],frame[37]);
		this.sourceIP = getSenderIP(frame,26,30);
		this.destIP = getTargetIP(frame,30,34);
		mediumsize = zratajPrenosMedium(size);
		this.type = type;
		this.protocol_ip_type = protocol_ip_type;
		this.port_type = port_type;
	}
	
	public void setFrame(String[] frame, int index, int size, String type,String protocol_ip_type,String port_type){
		this.frame = frame;
		this.index = index;
		this.size = size;
		this.type = type;
		this.protocol_ip_type = protocol_ip_type;
		this.port_type = port_type;
	}
	
	public void doStuffWithTFTP(ArrayList<Object> decoderList){
		String dest_port = getDecimalPort(frame[36], frame[37])+"";
		String source_port = getDecimalPort(frame[34], frame[35])+"";
		
		if(dest_port.equals(port_type)){
			if(portList.isEmpty() == false){
				tftpList.add(c);
			}
			c = new ArrayList<decoderTFTP>();
			portList.clear();
			portList.add(source_port);
			c.add(new decoderTFTP(frame, index, size, type, protocol_ip_type,("TFTP "+port_type)));			
		}
		else if(portList.isEmpty() == false){
			if(dest_port.equals(portList.get(0)) || source_port.equals(portList.get(0))){
				c.add(new decoderTFTP(frame, index, size, type, protocol_ip_type,("TFTP "+port_type)));
			}
		}
	}
	public void addLast(){
		tftpList.add(c);
	}
	
	public void printAllTFTP(JTextArea textArea){
				
		for (int j = 0; j < tftpList.size(); j++) {
			if(tftpList.get(j).isEmpty() == true)continue;
			textArea.append("Komunikacia c." + (j + 1) + "\n");
			if (tftpList.get(j).size() > 20) {
				for (int i = 0; i < 10; i++) {
					textArea.append("Zdrojova IP: "
							+ tftpList.get(j).get(i).sourceIP + ", ");
					textArea.append("Cielova IP: "
							+ tftpList.get(j).get(i).destIP + "\n");
					textArea.append("Zdrojovy port: "
							+ tftpList.get(j).get(i).source_port + ", ");
					textArea.append("Cielovy port: "
							+ tftpList.get(j).get(i).dest_port + "\n");
					textArea.append(tftpList.get(j).get(i).protocol_ip_type+"\n");
					textArea.append("Ramec c." + tftpList.get(j).get(i).index
							+ "\n");
					textArea.append("dĺžka rámca zachyteného paketovým drajverom: "
							+ tftpList.get(j).get(i).size + " B\n");
					textArea.append("dĺžka rámca prenášaného po médiu: "
							+ tftpList.get(j).get(i).mediumsize + " B\n");
					textArea.append("Ethernet II, " + tftpList.get(j).get(i).type+"\n");
					
					textArea.append("Well-known port: "+tftpList.get(j).get(i).port_type+"\n");
					textArea.append(tftpList.get(j).get(i).mergedFrame + "\n\n");

				}
				
				textArea.append("***************************************************\n");
				for (int i = tftpList.get(j).size() - 10; i < tftpList.get(j)
						.size(); i++) {
					textArea.append("Zdrojova IP: "
							+ tftpList.get(j).get(i).sourceIP + ", ");
					textArea.append("Cielova IP: "
							+ tftpList.get(j).get(i).destIP + "\n");
					textArea.append("Zdrojovy port: "
							+ tftpList.get(j).get(i).source_port + ", ");
					textArea.append("Cielovy port: "
							+ tftpList.get(j).get(i).dest_port + "\n");
					textArea.append(tftpList.get(j).get(i).protocol_ip_type+"\n");
					textArea.append("Ramec c." + tftpList.get(j).get(i).index
							+ "\n");
					textArea.append("dĺžka rámca zachyteného paketovým drajverom: "
							+ tftpList.get(j).get(i).size + " B\n");
					textArea.append("dĺžka rámca prenášaného po médiu: "
							+ tftpList.get(j).get(i).mediumsize + " B\n");
					textArea.append("Ethernet II, " + tftpList.get(j).get(i).type+"\n");
					
					textArea.append("Well-known port: "+tftpList.get(j).get(i).port_type+"\n");
					textArea.append(tftpList.get(j).get(i).mergedFrame + "\n\n");
				}
			} else {
				for (int i = 0; i < tftpList.get(j).size(); i++) {
					if(tftpList.get(j).isEmpty() == true)continue;
					textArea.append("Zdrojova IP: "
							+ tftpList.get(j).get(i).sourceIP + ", ");
					textArea.append("Cielova IP: "
							+ tftpList.get(j).get(i).destIP + "\n");
					textArea.append("Zdrojovy port: "
							+ tftpList.get(j).get(i).source_port + ", ");
					textArea.append("Cielovy port: "
							+ tftpList.get(j).get(i).dest_port + "\n");
					textArea.append(tftpList.get(j).get(i).protocol_ip_type+"\n");
					textArea.append("Ramec c." + tftpList.get(j).get(i).index
							+ "\n");
					textArea.append("dĺžka rámca zachyteného paketovým drajverom: "
							+ tftpList.get(j).get(i).size + " B\n");
					textArea.append("dĺžka rámca prenášaného po médiu: "
							+ tftpList.get(j).get(i).mediumsize + " B\n");
					textArea.append("Ethernet II, " + tftpList.get(j).get(i).type+"\n");
					
					textArea.append("Well-known port: "+tftpList.get(j).get(i).port_type+"\n");
					textArea.append(tftpList.get(j).get(i).mergedFrame + "\n\n");
				}
				textArea.append("***************************************************\n");
			}
		}
		
		clearAllTFTP();
	}
	
	public void clearAllTFTP(){
		tftpList.clear();
		portList.clear();
		c.clear();
	}
		
}
